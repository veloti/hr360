import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import App from './App';
import AllSurvey from '../pages/AllSurvey';
import Competence from '../pages/Competence';
import NewSurvey from "../pages/NewSurvey";

export default class MainRouter extends Component {
    constructor() {
        super();
        this.state = {
            navOpenState: {
                isOpen: true,
                width: 304,
            }
        }
    }

    getChildContext () {
        return {
            navOpenState: this.state.navOpenState,
        };
    }

    appWithPersistentNav = () => (props) => (
        <App
            onNavResize={this.onNavResize}
            {...props}
        />
    )

    onNavResize = (navOpenState) => {
        this.setState({
            navOpenState,
        });
    }

    render() {
        return (
            <Router history={browserHistory}>
                <Route component={this.appWithPersistentNav()}>
                    <Route path="/" component={AllSurvey} />
                    <Route path="/competence" component={Competence} />
                    <Route path="/new" component={NewSurvey} />
                </Route>
            </Router>
        );
    }
}

MainRouter.childContextTypes = {
    navOpenState: PropTypes.object,
}