import React from 'react';
import styled from 'styled-components';
import DynamicTable from '@atlaskit/dynamic-table';
import presidents from '../content/example/presidents.json'

const Wrapper = styled.div`
  min-width: 600px;
`;

const NameWrapper = styled.span`
  display: flex;
  align-items: center;
`;

function createKey(input) {
    return input ? input.replace(/^(the|a|an)/, '').replace(/\s/g, '') : input;
}

const rows = presidents.map((president, index) => ({
    key: `row-${index}-${president.nm}`,
    cells: [
        {
            key: createKey(president.nm),
            content: (
                <NameWrapper>
                    <a href="https://atlassian.design">{president.nm}</a>
                </NameWrapper>
            ),
        },
        {
            key: createKey(president.pp),
            content: president.pp,
        },
        {
            key: president.id,
            content: president.tm,
        },
        {
            content: "test",
        },
    ],
}));

export default class AllSurveyTable extends React.Component<{}, {}> {
    render() {
        return (
            <Wrapper>
                <DynamicTable
                    caption="test"
                    head={{
                        cells: [
                            {key: 0, content: "Name", isSortable: true},
                            {key: 1, content: "Date"},
                            {key: 2, content: "Due date"},
                            {key: 3, content: "Progress"},
                        ]
                    }}
                    rows={rows}
                    rowsPerPage={10}
                    defaultPage={1}
                    loadingSpinnerSize="large"
                    isLoading={false}
                    isFixedSize
                    defaultSortKey="term"
                    defaultSortOrder="ASC"
                    onSort={() => console.log('onSort')}
                    onSetPage={() => console.log('onSetPage')}
                />
            </Wrapper>
        );
    }
}