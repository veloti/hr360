import React from 'react';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/textfield';
import TextArea from '@atlaskit/textarea';
import Form, { Field, FormFooter } from '@atlaskit/form';

export default () => (
    <div
        style={{
            display: 'flex',
            width: '400px',
            margin: '0 auto',
            flexDirection: 'column',
        }}
    >
        <Form onSubmit={data => console.log(data)}>
            {({ formProps }) => (
                <form {...formProps} name="text-fields">
                    <Field name="surveyname" defaultValue="" label="Survey name" isRequired>
                        {({ fieldProps }) => <TextField {...fieldProps} />}
                    </Field>

                    <Field name="duedate" defaultValue="" label="Due date" isRequired>
                        {({ fieldProps: { isRequired, isDisabled, ...others } }) => (
                            <TextField
                                disabled={isDisabled}
                                required={isRequired}
                                {...others}
                            />
                        )}
                    </Field>

                <FormFooter>
                <Button type="submit" appearance="primary">
                Submit
                </Button>
                </FormFooter>
                </form>
                )}
        </Form>
    </div>
);