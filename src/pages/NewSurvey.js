import React, { Component } from 'react';
import PageTitle from "../components/PageTitle";
import ContentWrapper from "../components/ContentWrapper";
import Button, { ButtonGroup } from '@atlaskit/button';
import styled from 'styled-components';
import NewSurveyForm from "../components/NewSurveyForm";

const ButtonSpace = styled.div`
    display: flex;
    justify-content: space-between;
    width: 20%;
`

export default class NewSurvey extends Component {

    render() {
        return (
            <ContentWrapper>
                <PageTitle>New survey</PageTitle>
                <ButtonSpace>
                    <Button
                        appearance="primary"
                    > Save </Button>
                    <Button
                        appearance="default"
                    > Cancel </Button>
                </ButtonSpace>
                <hr/>
                <NewSurveyForm/>

            </ContentWrapper>
        )
    }


}