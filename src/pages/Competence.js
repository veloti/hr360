import React, { Component } from 'react';
import MainSection from '../components/MainSection';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';
import CompetenceMenu from "../components/CompetenceMenu";

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <CompetenceMenu/>
      </ContentWrapper>
    );
  }
}
